package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IrctcGetTrainNames {

	public static void main(String[] args) throws InterruptedException {
		// Get all train names
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//Launch the browser
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MDU",Keys.TAB);
		Thread.sleep(5000);
		WebElement trainTable = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> trainRows = trainTable.findElements(By.tagName("tr"));
		for(int i=0;i<trainRows.size();i++) {
			WebElement trainNames = trainRows.get(i);
			List<WebElement> printTrain = trainNames.findElements(By.tagName("td"));
			String eachTrain = printTrain.get(1).getText();
			System.out.println(eachTrain);
		}
	}

}
