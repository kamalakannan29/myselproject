package week3.day1;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcSignUp {

	public static void main(String[] args) throws InterruptedException {
		// IRCTC SignUp
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//Launch the browser
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		//Fill the required details
		driver.findElementById("userRegistrationForm:userName").sendKeys("1_2_irctc");
		driver.findElementById("userRegistrationForm:password").sendKeys("aAbB123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("aAbB123");
		WebElement varSecQue = driver.findElementById("userRegistrationForm:securityQ");
		Select secQue = new Select(varSecQue);
		secQue.selectByIndex(1);
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("TestAnswer");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("TestFirstName");
		List<WebElement> varGender = driver.findElementsByName("userRegistrationForm:gender");
		for(WebElement gender:varGender) {
			if(gender.getAttribute("value").equals("M")) {
				gender.click();
				break;
			}
		}
		List<WebElement> varMarital = driver.findElementsByName("userRegistrationForm:maritalStatus");
		for(WebElement Marital:varMarital) {
			if(Marital.getAttribute("value").equals("M")) {
				Marital.click();
				break;
			}
		}
		//-------------------------------------------------------------------------------------------------
		/*WebElement varDay = driver.findElementById("userRegistrationForm:dobDay");
		Select day = new Select(varDay);
		day.selectByVisibleText("21");
		WebElement varMonth = driver.findElementById("userRegistrationForm:dobMonth");
		Select month = new Select(varMonth);
		month.selectByVisibleText("NOV");
		WebElement varYear = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select year = new Select(varYear);
		year.selectByVisibleText("1990");*/
		//varSelect("userRegistrationForm:dobDay","21");
		varSelect("userRegistrationForm:dobMonth","NOV");
		varSelect("userRegistrationForm:dateOfBirth","1990");
		varSelect("userRegistrationForm:occupation","Private");
		varSelect("userRegistrationForm:countries","India");
		driver.findElementById("userRegistrationForm:email").sendKeys("test@gmail.com");
		driver.findElementById("userRegistrationForm:email").sendKeys("9876543210");
		varSelect("userRegistrationForm:nationalityId","India");
		driver.findElementById("userRegistrationForm:address").sendKeys("1,Test Address");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("603202",Keys.TAB);
		Thread.sleep(5000);
		varSelect("userRegistrationForm:cityName","Kanchipuram");
		varSelect("userRegistrationForm:postofficeName","Guduvanchery S.O");
		driver.findElementById("userRegistrationForm:landline").sendKeys("044-27461234");
	}
	public static void varSelect(String varID,String varSelect) {
		ChromeDriver driver = new ChromeDriver();
		WebElement var1 = driver.findElementById(varID);
		Select var2 = new Select(var1);
		var2.selectByVisibleText(varSelect);
	}
}
