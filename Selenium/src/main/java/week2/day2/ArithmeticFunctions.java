package week2.day2;

import java.util.Scanner;
public class ArithmeticFunctions {
	public static void main(String[] args) {
		// Arithmetic Functions
		Arithmetic();
	}
	public static void Arithmetic() {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter value1:");
		int input1 = input.nextInt();
		System.out.println("Enter value2:");
		int input2 =input.nextInt();
		System.out.println("Enter the function(Add/Sum/Mul/Div):");
		String Fun = input.next();
		switch (Fun) {
		case "Add":
			System.out.println("Addition of two numbers is:"+(input1+input2));
			break;
		case "Sub":
			System.out.println("Difference of two numbers is:"+(input1-input2));
			break;
		case "Mul":
			System.out.println("Multiplication of two numbers is:"+(input1*input2));
			break;
		case "Div":
			System.out.println("Division of two numbers is:"+(input1/input2)+" and the remainder is:"+(input1%input2));
			break;
		default:
			System.out.println("You have entry is invalid!");
			break;
		}
	input.close();	
	}
}
