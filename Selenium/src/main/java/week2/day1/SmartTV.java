package week2.day1;

public class SmartTV extends TeleVision{
	public void listenMusic() {
		System.out.println("Listen music in SmartTV");
	}
	public void watchYouTube() {
		System.out.println("Watching YouTube in SmartTV");
	}
	public void recordShow() {
		System.out.println("Recording show in SmartTV");
	}
	public static void main(String[] args) {
		//Creating object for TeleVision
		TeleVision TV = new TeleVision();
		SmartTV STV = new SmartTV();
		TV.channelTune();
		STV.recordShow();
		
	}
}
