package week2.day1;

public interface Entertainment {
	public void frequencyCheck();
	public void screenSize();
	public void resolution();
}
