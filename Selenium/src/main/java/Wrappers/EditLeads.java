package Wrappers;
	import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
	import org.testng.annotations.Test;
	public class EditLeads extends ProjectMethods{
		//@Test(groups= {"sanity"},dependsOnGroups= {"smoke"})
		@Test(groups= {"sanity"})
		public void editLeads() {
			try {
				WebElement crmsfaLink = locateElement("linktext","CRM/SFA");
				clickWithoutSnap(crmsfaLink);
				click(locateElement("linktext","Leads"));
				//String leadID = getText(locateElement("xpath", "((//table[contains(@class,'row-table')])[1]//a)[1]"));
				click(locateElement("xpath", "((//table[contains(@class,'row-table')])[1]//a)[1]"));
				click(locateElement("linktext","Edit"));
				click(locateElement("name", "submitButton"));
			}catch (WebDriverException e) {
				reportStep("Fail", "Edit Leads failed");
		}
	}
}
