package Wrappers;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
//import org.testng.annotations.DataProvider;
/*import org.openqa.selenium.WebElement;*/
import org.testng.annotations.Test;

public class MergeLeads extends ProjectMethods{
	//@Test(timeOut=15000)
	//@Test(groups= {"regression"},dependsOnGroups= {"sanity"})
	@Test(groups= {"regression"})
	public void mergeLeads() throws InterruptedException {
		try {
			WebElement crmsfaLink = locateElement("linktext","CRM/SFA");
			clickWithoutSnap(crmsfaLink);
			click(locateElement("linktext","Leads"));
			click(locateElement("linktext","Merge Leads"));
			click(locateElement("xpath", "//a[contains(@href,'partyIdFrom')]"));
			Set<String> allWin = driver.getWindowHandles();
			List<String> wins = new ArrayList<>();
			wins.addAll(allWin);
			driver.switchTo().window(wins.get(1));
			//String leadID1 = getText(locateElement("path", "((//table[contains(@class,'row-table')])[1]//a)[1]"));
			/*String leadID1 = getText(locateElement("xpath", "(//a[@class='linktext'])[1]"));
			type(locateElement("name", "id") ,leadID1);
			click(locateElement("xpath", "//button[text()='Find Leads']"));
			Thread.sleep(2000);*/
			click(locateElement("xpath", "(//a[@class='linktext'])[1]"));
			Thread.sleep(1000);
			driver.switchTo().window(wins.get(0));
			//Select ToLead
			click(locateElement("xpath", "//a[contains(@href,'partyIdTo')]"));
			Thread.sleep(1000);
			Set<String> allWin1 = driver.getWindowHandles();
			List<String> wins1 = new ArrayList<>();
			wins.addAll(allWin1);
			driver.switchTo().window(wins1.get(1));		
			click(locateElement("xpath", "(//a[@class='linktext'])[2]"));
			Thread.sleep(1000);
			driver.switchTo().window(wins.get(0));
			driver.close();	
			driver.switchTo().window(wins1.get(0));
			click(locateElement("linktext", "Merge"));
			acceptAlert();
		}catch (WebDriverException e) {
			reportStep("Fail", "Merge Leads failed");
		}
	}				
}