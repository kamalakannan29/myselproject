package week4.day2;
	import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import Wrappers.ProjectMethods;
	public class DuplicateLeads extends ProjectMethods{
		@Test
		public void duplicateLeads() {
			//login();
			WebElement crmsfaLink = locateElement("linktext","CRM/SFA");
			clickWithoutSnap(crmsfaLink);
			click(locateElement("linktext","Leads"));
			click(locateElement("linktext","Find Lead"));
			type(locateElement("createLeadForm_companyName"),"Wipro");
			//click(locateElement("xpath", "//a[contains(@href,'parent')]"));
			type(locateElement("createLeadForm_firstName"),"Kokki");
			type(locateElement("createLeadForm_lastName"),"Kumar");
			selectDropDownUsingText(locateElement("createLeadForm_dataSourceId"), "Employee");
			selectDropDownUsingIndex(locateElement("createLeadForm_marketingCampaignId"), 1);
			//click(locateElement("xpath", "//input[@value='Create Lead']"));
		}
}
