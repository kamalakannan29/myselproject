package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLeads {

	public static void main(String[] args) throws InterruptedException {
		//Login leaftaps site
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();;
		//click on CRM/SFA
		driver.findElementByLinkText("CRM/SFA").click();;
		driver.findElementByLinkText("Leads").click();;
		driver.findElementByLinkText("Merge Leads").click();;
		//Select FromLead
		driver.findElementByXPath("//a[contains(@href,'partyIdFrom')]").click();
		Set<String> allWin = driver.getWindowHandles();
		List<String> wins = new ArrayList<>();
		wins.addAll(allWin);
		driver.switchTo().window(wins.get(1));
		driver.findElementByName("firstName").sendKeys("Binu");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		Thread.sleep(1000);
		driver.switchTo().window(wins.get(0));
		//Select ToLead
		driver.findElementByXPath("//a[contains(@href,'partyIdTo')]").click();
		Thread.sleep(1000);
		Set<String> allWin1 = driver.getWindowHandles();
		List<String> wins1 = new ArrayList<>();
		wins.addAll(allWin1);
		driver.switchTo().window(wins1.get(1));		
		driver.findElementByName("firstName").sendKeys("Balaji");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.close();	
		driver.switchTo().window(wins1.get(0));
		driver.findElementByLinkText("Merge").click();
		//Handle Alert
		String text = driver.switchTo().alert().getText();
		System.out.println(text);
		driver.switchTo().alert().accept();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("firstName").sendKeys("Binu");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		boolean errMsg = driver.findElementByXPath("//div[text()='No records to display']").isDisplayed();
		if(errMsg==true){
			System.out.println("Verified Error Message");
		}
		driver.close();	
	}
	
}
