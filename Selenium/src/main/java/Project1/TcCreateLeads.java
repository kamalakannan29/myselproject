package Project1;
import org.openqa.selenium.WebElement;
/*import org.openqa.selenium.WebElement;*/
import org.testng.annotations.Test;

public class TcCreateLeads extends ProjectMethods{
	@Test
	public void createLeads() {
		WebElement crmsfaLink = locateElement("linktext","CRM/SFA");
		clickWithoutSnap(crmsfaLink);
		click(locateElement("linktext","Leads"));
		click(locateElement("linktext","Create Lead"));
		type(locateElement("createLeadForm_companyName"),"Wipro");
		//click(locateElement("xpath", "//a[contains(@href,'parent')]"));
		type(locateElement("createLeadForm_firstName"),"Kokki");
		type(locateElement("createLeadForm_lastName"),"Kumar");
		selectDropDownUsingText(locateElement("createLeadForm_dataSourceId"), "Employee");
		selectDropDownUsingIndex(locateElement("createLeadForm_marketingCampaignId"), 1);
		click(locateElement("xpath", "//input[@value='Create Lead']"));
	}
	
}
