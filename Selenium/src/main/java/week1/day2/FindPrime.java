package week1.day2;

import java.util.Scanner;

public class FindPrime {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int input,flag=0,i;
		Scanner getinput = new Scanner(System.in);
		System.out.println("Enter a number to check for prime:");
		input = getinput.nextInt();
		if(input==0||input==1) {
			System.out.println("Entered number "+input+" is not prime");
		}else {
			for(i=2;i<=input/2;i++) {
				if(input%i==0) {
					System.out.println("Entered number "+input+" is not prime");
					flag=0; 
					break;
				}else {
					flag=1;
				}
			}
			if(flag==1) {
				System.out.println("Entered number "+input+" is prime");
			}
		}
		getinput.close();
	}

}
